<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 */
?>
<header id="header" role="banner" class="clearfix">
  
  <div class="container">
    <div class="logo">
      <img src="../<?php print path_to_theme(); ?>/logo.png" />
    </div>
    
    <nav role="navigation">
      <?php print theme('links',array('links'=>$main_menu)); ?>
    </nav>
  </div>

</header>

<div class="main-container container clearfix">

  <header role="banner" id="page-header">
    <?php print render($page['header']); ?>
  </header> <!-- /#page-header -->
  
  <div class="content-holder clearfix">

    <!-- holder for above content region -->
    <?php if(!empty($page['above_content'])): ?>
      <div class="above-content">
        <?php print render($page['above_content']); ?>
      </div>
    <?php endif; ?>
  
    <!-- holder for first sidebar -->
    <div class="one-quarter width sidebar" role="complementary">
      <div class="inner">
        <?php print render($page['sidebar_first']); ?>
      </div>
    </div> 
  
    <div class="half width">
      <?php if (!empty($breadcrumb)): ?>
        <?php print $breadcrumb; ?>
      <?php endif;?>
      
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      
      <?php print render($title_suffix); ?>
      
      <!-- system holder for messages -->
      <?php if (!empty($messages)): ?>
        <?php print $messages; ?>
      <?php endif; ?>
      
      <!-- system holder for tabs -->
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      
      <!-- system holder for help -->
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      
      <!-- system holder for action links -->
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      
      <!-- main content here -->
      <?php print render($page['content']); ?>
    </div>
	
    <!-- holder for right sidebar -->
    <div class="one-quarter width sidebar" role="complementary">
      <div class="inner">
        <?php print render($page['right_sidebar']); ?>
      </div>
    </div> 	
	
  </div>
  
  <!-- holder for above content region -->
  <?php if(!empty($page['below_content'])): ?>
    <div class="below-content">
      <?php print render($page['below_content']); ?>
    </div>
  <?php endif; ?>

</div>


<!-- footer -->
<?php if (!empty($page['footer'])): ?>
  <footer class="footer">
    <?php print render($page['footer']); ?>
  </footer>
<?php endif; ?>
